module Main where

import Prelude hiding (head, group)

main :: IO ()
main = putStrLn $ unstutter "Hhhhhiiiiiiiiii Wwwwooooooooorrrrrrrllllld."


-- Below transcribed from liquidHaskell splash page
{-@ type NonEmpty a = {v:[a] | 0 < len v } @-}
{-@ head :: NonEmpty a -> a @-}
head :: [a] -> a
head (x:_) = x

{-@ group :: (Eq a) => [a] -> [ NonEmpty a ] @-}
group :: Eq a => [a] -> [[a]]
group [] = []
group (x:xs) = (x:ys) : group zs
  where
    (ys, zs) = span (x ==) xs

unstutter :: Eq a => [a] -> [a]
unstutter = map head . group
