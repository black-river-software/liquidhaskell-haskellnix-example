{ sources ? import nix/sources.nix
, pkgs ? import sources.nixpkgs
  { config = import "${sources.haskell-nix}/config.nix"; 
    overlays = import "${sources.haskell-nix}/overlays"
               ++ [ (_self: super: {
	               haskell-nix = super.haskell-nix // {
		         hackageSrc = sources.hackage-nix;
			 stackageSrc = sources.stackage-nix;
		       };})];}}:
let project = pkgs.haskell-nix.stackProject {
    src = ./.;
    modules = [ ({pkgs, lib, ...}: {
      config.packages.liquidhaskell-haskellnix-example.components.exes.liquidhaskell-haskellnix-example = {
        preBuild = ''
	  PATH=${pkgs.lib.makeBinPath [ pkgs.z3 ]}:$PATH
          ${liquid}/bin/liquid Main.hs
          '';
	doCheck = true;
       };
       config.reinstallableLibGhc = true;
    })];
  };
  liquid = pkgs.runCommand "liquid-wrapped" { nativeBuildInputs = [ pkgs.makeWrapper ]; } ''
    mkdir -p $out/bin
    makeWrapper "${project.liquidhaskell.components.exes.liquid}/bin/liquid" $out/bin/liquid \
      --prefix PATH : ${pkgs.lib.makeBinPath [ pkgs.z3 ]} \
      --add-flags "--ghc-option=-fexternal-interpreter"
  '';
in project.liquidhaskell-haskellnix-example.components.exes.liquidhaskell-haskellnix-example.overrideAttrs (_: {
  passthru = { inherit project pkgs liquid; };
})
