args@{...}:
let default = (import ./. args);
in default.project.shellFor {
  packages = ps: with ps; [ liquidhaskell-haskellnix-example ];
  buildInputs = [ default.liquid ];
}
