# Example of using liquidhaskell with haskell.nix

Copyright 2020 Black River Software

## Notes
* We put the call to `liquid` in the preBuild and not preCheck hook is (a) so we don't waste
time compiling if the liquid checks fail and (b) because haskell.nix ignores preCheck and postCheck in its
default derivations.

* Preferably there would be a way to add build inputs to to haskell.nix derivations via the module system.
Since there isn't, we just set the PATH variable to include `liquid` prior to
invoking `liquid`.

* Since `liquid` needs an SMT solver on the path, we do similarly for `z3`.

* It would seem something has in a ghc update since the last `liquid` update,
  since for ghc-8.6.5 we have to tell `liquid` to pass the `-fexternal-interpreter` flag.

* There are solutions to invoke `liquid` on all the haskell programs in a cabal package, but this example simply passes the source files on the command line.

